package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName(String loginName) {

		// Regex to check valid loginName.
		String regex = "^[A-Za-z]\\w{5,29}$";

		// Compile the ReGex
		Pattern bhharsha = Pattern.compile(regex);

		// If the loginName is empty
		// return false
		if (loginName == null) {
			return false;
		}

		Matcher harshal = bhharsha.matcher(loginName);

		return harshal.matches();
	}
}
