package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LoginValidatorTest {

	// validating for the first requirement
	@Test
	public void testIsValidLoginR1Regular() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("bhharsha22");
		assertTrue("Invalid login Name.", isValidLoginName);
	}

	@Test
	public void isValidLoginNameR1Exceptional() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("bhharsha@22");
		assertFalse("Invalid login Name.", isValidLoginName);
	}

	@Test
	public void isValidLoginNameR1BoundaryIn() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("h1ars22");
		assertTrue("Invalid login Name.", isValidLoginName);
	}

	@Test
	public void isValidLoginNameR1BoundaryOut() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("11b@22");
		assertFalse("Invalid login Name.", isValidLoginName);
	}

	// validating for the secoond requirement
	@Test
	public void testIsValidLoginR2Regular() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("bhharsha22");
		assertTrue("Invalid login Name.", isValidLoginName);
	}

	@Test
	public void isValidLoginNameR2Exceptional() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("bhh");
		assertFalse("Invalid login Name.", isValidLoginName);
	}

	@Test
	public void isValidLoginNameR2BoundaryIn() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("hars11");
		assertTrue("Invalid login Name.", isValidLoginName);
	}

	@Test
	public void isValidLoginNameR2BoundaryOut() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("b@22");
		assertFalse("Invalid login Name.", isValidLoginName);
	}
}
